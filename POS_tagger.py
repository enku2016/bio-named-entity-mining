import nltk
ps = nltk.stem.PorterStemmer()
from nltk.stem.wordnet import WordNetLemmatizer
lmtzr = WordNetLemmatizer()
from textblob import TextBlob
import sys
reload(sys)
sys.setdefaultencoding('utf8')


def tokenize_words(sentence):
    tokens = nltk.word_tokenize(sentence)
    pos_tokens= nltk.pos_tag(tokens)
    stems = []
    for token in tokens:
        stems.append(ps.stem(token))
    lematization = []
    for token in tokens:
        lematization.append(lmtzr.lemmatize(token))
    blob = TextBlob(sentence)
    noun_phrases = blob.noun_phrases
    return tokens , pos_tokens , stems , lematization , noun_phrases

