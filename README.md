# MOZI Bio-Entity Relationship Mining

## Overview 
Bio-Entity relationship mining aims to extract Biological Entities, which are `Gene`, `Protein`, `RNA`, `DNA`, `Cell Line`, `Gene` and `Disease` from different research papers and represent their relationships with each other and other parts of sentence in Scheme format. This is achieved by first extracting only the important text content from a PDF file, leaving behind the verbiage. Then parsing the extracted text sentence by sentence and look for any Bio Entities. After Bio Entities are found they will be represented in Scheme format.

## Architecture
We have used three libraries for successfully extracting bio-entities. The first library is `GROBIN`, a PDF Extractor library written in Java. `GROBIN` takes PDF files as input and returns an XML file annotating components such as Abstract, Reference, Main Body. From the XML file we take the ones that are interesting to our purpose. 
After the interesting parts are extracted from the PDF file, we then use a sentence tokenizer(`Stanford NLP`) to separate sentences. 

Then we feed each sentence to a part of speech tagger( `Reverb`, `Stanford NLP`) to extract the subject, object and predicate of the sentence. Results from the part-of-speech tagger are then fed into a bio-relationship extractor(`ABNER`) which will look for any bio named entities and tag them. Then we create atomspace representation using Evaluation-Link for the subject and Object together with the verb as a predicate. This relationship extracted from a pdf document will then be visualized through `MOZI` visualizer and will also be imported into the bio-atomspace to have a richer knowledge base.  

## Installation

Make sure you have `python 2.7` installed on your system.

### Required Libraries

- [GROBID](http://grobid.readthedocs.io/en/latest/Introduction/) is a machine learning library for extracting, parsing and re-structuring raw documents such as PDF into structured TEI-encoded documents with a particular focus on technical and scientific publications. 
- [FLASK](http://flask.pocoo.org/) is a microframework for Python based on Werkzeug, Jinja 2 and good intentions.

#### Installing GROBID
Download latest version of GROBID
```sh
$ wget https://github.com/kermitt2/grobid/archive/grobid-parent-0.4.1.zip
$ unzip grobid-grobid-parent-0.4.1.zip
```
Build GROBID
Please make sure that grobid is installed in your home directory(~).The standard method for building GROBID is to use maven. 
Under the main directory `grobid/`:
```sh
$ mvn clean install
```
You can skip the tests as follow:
```sh
$ mvn -Dmaven.test.skip=true clean install
```
or:
```sh
$ mvn -DskipTests=true clean install
```
#### Installing FLASK
Install using pip
```sh
$ pip install Flask
```
### Clone Repo
```sh
git clone https://gitlab.com/enku2016/bio-named-entity-mining.git
```
## Run Service
```sh
$ FLASK_APP=server.py 
$ flask run
```
