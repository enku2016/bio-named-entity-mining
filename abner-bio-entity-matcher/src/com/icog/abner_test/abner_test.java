package com.icog.abner_test;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Vector;

import org.omg.CORBA.Any;
import org.omg.CORBA.Object;
import org.omg.CORBA.TypeCode;
import org.omg.CORBA.portable.InputStream;
import org.omg.CORBA.portable.OutputStream;

import abner.Tagger;

public class abner_test {
	static Tagger tagger;
	static String demo1 = "Analysis of myeloid-associated genes in human hematopoietic progenitor cells.\nBello-Fernandez et al. Exp Hematol.  1997 Oct;25(11):1158-66.\n\nThe distribution of myeloid lineage-associated cytokine receptors and lysosomal proteins was analyzed in human CD34+ cord blood cell (CB) subsets at different stages of myeloid commitment by reverse-transcriptase polymerase chain reaction (RT-PCR). The highly specific granulomonocyte-associated lysosomal proteins myeloperoxidase (MPO) and lysozyme (LZ), as well as the transcription factor PU.1, were already detectable in the most immature CD34+Thy-1+ subset. Messenger RNA (mRNA) levels for the granulocyte-colony stimulating factor (G-CSF) receptor, granulocyte-macrophage (GM)-CSF receptor alpha subunit and tumor necrosis factor (TNF) receptors I (p55) and II (p75) were also detected in this subset in addition to c-kit and flt-3, receptors known to be expressed on progenitor cells. By contrast, the monocyte-macrophage colony stimulating factor (M-CSF) receptor was largely absent at this stage and in the CD34+Thy-1-CD45RA- subsets. The M-CSF receptor was first detectable in the myeloid-committed CD34+Thy-l-CD45RA+ subset. All other molecules studied were found to be expressed at this stage of differentiation. Different cocktails of the identified ligands were added to sorted CD34+Thy-1+ single cells. Low proliferative capacity was observed after 1 week in culture in the presence of stem cell factor (SCF) + Flt-3 ligand (FL) + G-CSF. Addition of GM-CSF to this basic cocktail consistently increased the clonogenic capacity of single CD34+Thy-1+ cells, and this effect was further enhanced (up to 72.3 +/- 4.3% on day 7) by the inclusion of TNF-alpha. In conclusion, the presence of myeloid-associated growth factor receptor transcripts in CD34+ CB subsets does not discriminate the various stages of differentiation, with the exception of the M-CSF receptor. In addition, we show that TNF-alpha is a potent costimulatory factor of the very immature CD34+Thy-1+ CB subset.";
	static String demo2 = "AP-1 (Fos/Jun) transcription factors in hematopoietic differentiation and apoptosis.\nLiebermann et al. Int J Oncol.  1998 Mar;12(3):685-700.\n\nA combination of in vitro and in vivo molecular genetic approaches have provided evidence to suggest that AP-1 (Fos/Jun) transcription factors play multiple roles in functional development of hematopoietic precursor cells into mature blood cells along most, if not all, of the hematopoietic cell lineages. This includes the monocyte/macrophage, granulocyte, megakaryocyte, mastocyte and erythroid lineages. In addition, studies using c-Fos knockout mice have established a unique role for Fos, as a member of the AP-1 transcription factor complex, in determining the differentiation and activity of progenitors of the osteoclast lineage, a population of bone-forming cells which are of hematopoietic origin as well. Evidence has also accumulated to implicate AP-1 (Fos/Jun) transcription factor complexes as both positive and negative modulators of distinct apoptotic pathways in many cell types, including cells of hematopoietic origin. Fos/Jun have been implicated as positive modulators of apoptosis induced in hematopoietic progenitor cells of the myeloid lineage, a function that may relate to the control of blood cell homeostasis, as well as in programmed cell death associated with terminal differentiation of many other cell types, and apoptosis associated with withdrawal of growth/survival factors. On the other hand, the study of apoptosis induced in mammalian cells has implicated AP-1 in the protection against apoptosis induced by DNA-damaging agents. However, evidence to the contrary has been obtained as well, suggesting that AP-1 may function to modulate stress-induced apoptosis either positively or negatively, depending on the microenvironment and the cell type in which the stress stimulus is induced.";
	static String demo3 = "Heterozygous PU.1 mutations are associated with acute myeloid leukemia.\nMueller et al. Blood. 2002 Aug 1;100(3):998-1007.\n\nThe transcription factor PU.1 is required for normal blood cell development. PU.1 regulates the expression of a number of crucial myeloid genes, such as the macrophage colony-stimulating factor (M-CSF) receptor, the granulocyte colony-stimulating factor (G-CSF) receptor, and the granulocyte-macrophage colony-stimulating factor (GM-CSF) receptor. Myeloid cells derived from PU.1(-/-) mice are blocked at the earliest stage of myeloid differentiation, similar to the blast cells that are the hallmark of human acute myeloid leukemia (AML). These facts led us to hypothesize that molecular abnormalities involving the PU.1 gene could contribute to the development of AML. We identified 10 mutant alleles of the PU.1 gene in 9 of 126 AML patients. The PU.1 mutations comprised 5 deletions affecting the DNA-binding domain, and 5 point mutations in 1) the DNA-binding domain (2 patients), 2) the PEST domain (2 patients), and 3) the transactivation domain (one patient). DNA binding to and transactivation of the M-CSF receptor promoter, a direct PU.1 target gene, were deficient in the 7 PU.1 mutants that affected the DNA-binding domain. In addition, these mutations decreased the ability of PU.1 to synergize with PU.1-interacting proteins such as AML1 or c-Jun in the activation of PU.1 target genes. This is the first report of mutations in the PU.1 gene in human neoplasia and suggests that disruption of PU.1 function contributes to the block in differentiation found in AML patients.";
	  
	public static void main(String[] args) {
		String taggerModel = args[0];
		int taggerModelNumber = 0;
		try {
			taggerModelNumber = Integer.parseInt(taggerModel);
		}catch(Exception e) {
			System.err.print(e);
			return;
		}
		if(taggerModelNumber!= 0 && taggerModelNumber!= 1) {
			System.err.print("Please provide 0 and 1 as arguments for NLPBA and BIOCREATIVE models respectively");
			return;
		}
		try{
			tagger = new Tagger(taggerModelNumber);
			String mainArg = new String();
			for(int x = 1 ; x < args.length; x++) {
				mainArg = mainArg.concat(args[x] + ' ');
			}
			Vector res = tagger.getSegments(mainArg);
			String doc = new String();
			for (int i = 0; i < res.size(); i++) {
				String[][] sent = (String[][])res.get(i);
				for (int j = 0; j < sent[0].length; j++) {
					if(sent[1][j].equalsIgnoreCase("O")) {
						sent[1][j] = "";
						System.out.print(sent[0][j]);
					}else {
						System.out.print(" <"+sent[1][j]+"> " +sent[0][j] + " </"+sent[1][j]+"> ");
					}
				}
				System.out.print("\n");

			}
		}catch( Exception e){
			e.printStackTrace();
		}

		
	}
}
