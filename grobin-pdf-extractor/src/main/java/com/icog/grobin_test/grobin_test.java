package com.icog.grobin_test;
import org.grobid.core.engines.config.GrobidAnalysisConfig;
import org.grobid.core.factory.*;
import org.grobid.core.mock.*;
import org.grobid.core.utilities.*;
import org.grobid.core.engines.Engine;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import java.io.*;
import java.io.File;
import edu.stanford.nlp.simple.*;
public class grobin_test {

    private static final String pdfPath = "/home/icog-labs/Documents/Work/iCog/text-preporcessing/lit/longevity_lit/";
    private static final String textDumpPath = "/home/icog-labs/Documents/Work/iCog/text-preporcessing/lit//processed_text/";
    private static final String pGrobidHome = "/home/icog-labs/grobid-grobid-parent-0.4.1/grobid-home";
    private static final String pGrobidProperties = "/home/icog-labs/grobid-grobid-parent-0.4.1/grobid-home/config/grobid.properties";

    public static void main(String [] args){
        try{
            String fileLocation = args[0];
            if(fileLocation == null){
                System.err.println("Please provide file location");
            }else{
                Engine engine = InstantiateEngine();
                System.out.println("Processing " + args[0]);
                GrobidAnalysisConfig config = getGrobidConfigFile();
                File file = new File(args[0]);
                String xml = engine.fullTextToTEI(file,config);
                String result = parseXML(xml);
                edu.stanford.nlp.simple.Document doc = new edu.stanford.nlp.simple.Document(result);

                for (Sentence sent : doc.sentences()) {  // Will iterate over two sentences
                    System.out.println(preprocessSentence(sent.toString())+'\n');
                }
            }
        }
        catch (Exception e) {
            // If an exception is generated, print a stack trace
            e.printStackTrace();
        }
        finally {
            try {
                MockContext.destroyInitialContext();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static String parseXML(String xml) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        StringBuilder xmlStringBuilder = new StringBuilder();
        xmlStringBuilder.append(xml);
        ByteArrayInputStream input =  new ByteArrayInputStream(xmlStringBuilder.toString().getBytes("UTF-8"));
        Document doc = builder.parse(input);
        doc.getDocumentElement().normalize();
        NodeList nList = doc.getElementsByTagName("p");
        String result = new String();
        for(int i = 0; i < nList.getLength();i++){
            Node node = nList.item(i);
            Element e = (Element)node;
            result = result.concat("\n");
            result = result.concat(e.getTextContent());
        }
        nList = doc.getElementsByTagName("title");
        for(int i = 0; i < nList.getLength();i++){
            Node node = nList.item(i);
            Element e = (Element)node;
            result = result.concat("\n");
            result = result.concat(e.getTextContent());
        }
        nList = doc.getElementsByTagName("note");
        for(int i = 0; i < nList.getLength();i++){
            Node node = nList.item(i);
            Element e = (Element)node;
            result = result.concat("\n");
            result =  checkTagStringLength(e.getTextContent())?result.concat(e.getTextContent()):result;
        }
        return result;
    }

    private static Engine InstantiateEngine() throws Exception {
        MockContext.setInitialContext(pGrobidHome, pGrobidProperties);
        GrobidProperties.getInstance();

        System.out.println(">>>>>>>> GROBID_HOME="+GrobidProperties.get_GROBID_HOME_PATH());
        return GrobidFactory.getInstance().createEngine();
    }

    private static String[] getPDFFiles(){
        File PDFFilesDirectory = new File(pdfPath);
        String[] pdfFiles = PDFFilesDirectory.list(new FilenameFilter(){
            public boolean accept(File file, String s) {
                if(s.contains(".pdf")){
                    return true;
                }
                return false;
            }
        });
        return pdfFiles;
    }

    private static GrobidAnalysisConfig getGrobidConfigFile(){
        GrobidAnalysisConfig.GrobidAnalysisConfigBuilder builder = new GrobidAnalysisConfig.GrobidAnalysisConfigBuilder();
        builder.startPage(1);
        builder.withPreprocessImages(false);
        builder.withProcessVectorGraphics(false);
        builder.consolidateCitations(false);
        builder.consolidateHeader(false);
        return builder.build();
    }
    private static String preprocessSentence(String sentence){
        sentence = sentence.replaceAll("\\[(.*?)\\]","");
        sentence = sentence.replaceAll("\\((.*?)\\)","");
        return sentence;
    }
    private static String extractFileName(String fileName){
        String[] tokens = fileName.split("\\.(?=[^\\.]+$)");
        return tokens[0];
    }
    private static boolean checkTagStringLength(String tagContent){
        return tagContent.length()>100;
    }
}
