import glob
from subprocess import Popen, PIPE, STDOUT
import os
import thread

def processReverb(file_name,socketio):
    defineVars()
    thread.start_new_thread( sendProgressReport,(socketio,"Processing "+file_name))
    IE = Popen(['java', '-jar', 'lib/reverb-latest.jar', file_name],stdout=PIPE, stderr=STDOUT)

    for line in IE.stdout:
        full_text = line.split('\t')
        big_list.append(full_text)

    for i in  big_list[5:-2]:
        subject_list.append(i[2])
        predicate_list.append(i[3])
        object_list.append(i[4])

    count = 0
    count2 = 0
    result = ''
    while(not count > len(subject_list) - 1):

        final_list , shouldBePutSub , shouldBePutOb = processAbner(subject_list, object_list , count , count2)

        if(shouldBePutOb or shouldBePutSub):
            resulttemp = processBioEntities(final_list,count,count2)
            result = result + '\n' + str(resulttemp)
            count2 =  count2 + 1

        count = count + 1
    return result