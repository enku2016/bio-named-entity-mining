import glob
from subprocess import Popen, PIPE, STDOUT
import os
import thread


def defineVars():
    global list_of_files 
    global scheme_template
    global scheme_template_multiple 
    global scheme_or_template
    global subject_big_list
    global object_big_list
    global big_list
    global subject_list
    global predicate_list
    global object_list
    global final_list

    list_of_files = glob.glob('complex-sentence-test.txt')
    scheme_template = "(EvaluationLink" + "\n\t" +"(PredicateNode \"$$\")" + "\n\t" + "(ListLink" + "\n\t\t" +   "(@@ \"%%\")" + "\n\t\t" + "(** \"##\")"  + "\n\t" + ")" + "\n"+ ")" + "\n"
    scheme_template_multiple = "(EvaluationLink" + "\n\t" +"(PredicateNode \"$$\")" + "\n\t" + "(ListLink" + "\n\t\t" +   "@@" + "\n\t\t" + "##"  + "\n\t" + ")" + "\n"+ ")" + "\n"
    scheme_or_template = "(OrLink " + "\n\t\t\t" + "@@"+"\n\t\t)"

    subject_big_list = []
    object_big_list = []
    big_list = []
    subject_list = []
    predicate_list = []
    object_list = []
    final_list = []
def countBioEntityOccurence(sampleString):
    return sampleString.count("<PROTEIN>") + sampleString.count("<CELL_LINE>")  + sampleString.count("<DNA>") + sampleString.count("<RNA>") + sampleString.count("<CELL_TYPE>")
def generateSchemeFromSubjects(final_list,count2,result):
    BioEFound = False
    BioEFound2 = False
    BioEntitySub = ""
    SubWOBioEntity = ""
    result = scheme_template
    cnt = 0

    for sub in final_list[count2][0]:
        if ("\"" in sub):
            continue
        elif("<protein>" in sub.lower()):
            BioEFound = True
            BioEFound2 = True
            result = result.replace("@@","ProteinNode")
        elif("<dna>" in sub.lower()):
            BioEFound = True
            BioEFound2 = True
            result = result.replace("@@","DnaNode")
        elif("<rna>" in sub.lower()):
            BioEFound = True
            BioEFound2 = True
            result = result.replace("@@","RnaNode")
        elif("<cell_line>" in sub.lower()):
            BioEFound = True
            BioEFound2 = True
            result = result.replace("@@","CellLineNode")
        elif("<cell_type>" in sub.lower()):
            BioEFound = True
            BioEFound2 = True    
            result = result.replace("@@","CellTypeNode") 
        elif("</protein>" in sub.lower()):
            BioEFound = False
        elif("</dna>" in sub.lower()):
            BioEFound = False
        elif("</rna>" in sub.lower()):
            BioEFound = False
        elif("</cell_line>" in sub.lower()):
            BioEFound = False
        elif("</cell_type>" in sub.lower()):
            BioEFound = False
        elif(BioEFound):
            BioEntitySub += " " + sub
        elif(not BioEFound2):
            SubWOBioEntity +=  " " +  sub
        # print BioEFound , sub , " <=> ",  BioEntitySub , " cnt : " , cnt
        cnt += 1
    result = result.replace("$$",' '.join(final_list[count2][1]))
    result = result.replace("%%",BioEntitySub if BioEFound2 else SubWOBioEntity)
    result = result.replace("@@","ConceptNode" if not BioEFound2 else "")
    return result
def generateSchemeFromObjects(final_list,count2,result):
    BioEFound = False
    BioEFound3 = False
    BioEntityOb = ""
    ObWOBioEntity = ""
    for Ob in final_list[count2][2]:
        if ("\"" in Ob):
            continue
        elif("<protein>" in Ob.lower()):
            BioEFound = True
            BioEFound3 = True
            result = result.replace("**","ProteinNode")
        elif("<dna>" in Ob.lower()):
            BioEFound = True
            BioEFound3 = True
            result = result.replace("**","DnaNode")
        elif("<rna>" in Ob.lower()):
            BioEFound = True
            BioEFound3  = True
            result = result.replace("**","RnaNode")
        elif("<cell_line>" in Ob.lower()):
            BioEFound = True
            BioEFound3  = True
            result = result.replace("**","CellLineNode")
        elif("<cell_type>" in Ob.lower()):
            BioEFound = True
            BioEFound3  = True    
            result = result.replace("**","CellTypeNode") 
        elif("</protein>" in Ob.lower()):
            BioEFound = False
        elif("</dna>" in Ob.lower()):
            BioEFound = False
        elif("</rna>" in Ob.lower()):
            BioEFound = False
        elif("</cell_line>" in Ob.lower()):
            BioEFound = False
        elif("</cell_type>" in Ob.lower()):
            BioEFound = False
        elif(BioEFound):
            BioEntityOb += " " + Ob
        elif(not BioEFound3):
            ObWOBioEntity +=  " " +  Ob
    result = result.replace("##",BioEntityOb if BioEFound3 else ObWOBioEntity)
    result = result.replace("**","ConceptNode" if not BioEFound3 else "")
    return result
def generateSchemeFromMultipleSubjects(final_list,count2,result):
    # print final_list[count2][0]
    result = result.replace("@@",scheme_or_template)
    index = 0
    for sub in final_list[count2][0]:
        if("\"" in sub):
            continue
        elif("<PROTEIN>" in sub):
            result = result.replace("@@","(ProteinNode \"" + final_list[count2][0][index+1] + "\")" + "\n\t\t\t" + "@@")
        elif("<CELL_TYPE>" in sub):
            result = result.replace("@@","(CellTypeNode \"" + final_list[count2][0][index+1] + "\")" +  "\n\t\t\t" + "@@")
        elif("<CELL_LINE>" in sub):
            result = result.replace("@@","(CellLineNode \"" + final_list[count2][0][index+1] + "\")" +  "\n\t\t\t" + "@@")
        elif("<DNA>" in sub):
            result = result.replace("@@","(DnaNode \"" + final_list[count2][0][index+1] + "\")" +  "\n\t\t\t" + "@@")
        elif("<RNA>" in sub):
            result = result.replace("@@","(DnaNode \"" + final_list[count2][0][index+1] + "\")" +  "\n\t\t\t" + "@@")
        if(index == len(final_list[count2][0])-1):
            result = result.replace("@@","")
        index = index + 1
    result = result.replace("$$",' '.join(final_list[count2][1]))
    return result        
def processBioEntities(BioEntityArray,count,count2):

    subBioEOccurence = countBioEntityOccurence(' '.join(BioEntityArray[count2][0]))
    obBioEOccurence  = countBioEntityOccurence(' '.join(BioEntityArray[count2][2]))
    # print "sub" , subBioEOccurence , "ob" , obBioEOccurence
 
    if(subBioEOccurence < 2 and obBioEOccurence < 2):
        result = scheme_template
        result = generateSchemeFromSubjects(final_list,count2,result)
        result = generateSchemeFromObjects(final_list,count2,result)
        return result
    elif(subBioEOccurence > 1 and obBioEOccurence < 2):
        result = scheme_template_multiple
        result = generateSchemeFromMultipleSubjects(final_list,count2,result)
        result = result.replace("##","(** \"##\")")
        result = generateSchemeFromObjects(final_list,count2,result)
        return result
def processAbner(subject_list,object_list,count,count2):    
    ABNER_Subject = Popen(['java', '-jar', 'lib/abner_test.jar', '0', subject_list[count]], stdout=PIPE, stderr=STDOUT)
    ABNER_Object  = Popen(['java', '-jar', 'lib/abner_test.jar', '0', object_list[count]], stdout=PIPE, stderr=STDOUT)

    shouldBePutSub = False
    shouldBePutOb  = False

    final_list.append([[],[],[]])
    for i in ABNER_Subject.stdout:
        if ("Logging configuration class \"edu.umass.cs.mallet.base.util.Logger.DefaultConfigurator\" failed" not in i) and ("Loading BioCreative tagging module..." not in i) and ("Loading NLPBA tagging module..." not in i)  and ("java.lang.ClassNotFoundException: edu.umass.cs.mallet.base.util.Logger.DefaultConfigurator" not in i):
            split = i.split()
            if( ("<protein>" in i.lower()) or ("<dna>"  in i.lower()) or ("<rna>"  in i.lower()) or ("<cell_line>"  in i.lower()) or  ("<cell_type>"  in i.lower())):
                shouldBePutSub = True
                final_list[count2][0] = split
                final_list[count2][1] = predicate_list[count].split()

    for i in ABNER_Object.stdout:
        if ("Logging configuration class \"edu.umass.cs.mallet.base.util.Logger.DefaultConfigurator\" failed" not in i) and ("Loading BioCreative tagging module..." not in i) and ("Loading NLPBA tagging module..." not in i)  and ("java.lang.ClassNotFoundException: edu.umass.cs.mallet.base.util.Logger.DefaultConfigurator" not in i):
            split = i.split()
            if( ("<protein>" in i.lower()) or ("<dna>"  in i.lower()) or ("<rna>"  in i.lower()) or ("<cell_line>"  in i.lower()) or  ("<cell_type>"  in i.lower())):
                shouldBePutOb = True
                final_list[count2][2] = split
                if(not shouldBePutSub):
                    final_list[count2][0] = subject_list[count].split()
                    final_list[count2][1] = predicate_list[count].split()
            if(shouldBePutSub and not shouldBePutOb):
                final_list[count2][2] = split
    return final_list , shouldBePutSub , shouldBePutOb
def processReverb(file_name,socketio):
    defineVars()
    IE = Popen(['java', '-jar', 'lib/reverb-latest.jar', file_name],stdout=PIPE, stderr=STDOUT)

    for line in IE.stdout:
        full_text = line.split('\t')
        big_list.append(full_text)

    for i in  big_list[5:-2]:
        subject_list.append(i[2])
        predicate_list.append(i[3])
        object_list.append(i[4])

    count = 0
    count2 = 0
    result = ''
    while(not count > len(subject_list) - 1):

        final_list , shouldBePutSub , shouldBePutOb = processAbner(subject_list, object_list , count , count2)

        if(shouldBePutOb or shouldBePutSub):
            resulttemp = processBioEntities(final_list,count,count2)
            result = result + '\n' + str(resulttemp)
            count2 =  count2 + 1

        count = count + 1
    return result
def sendProgressReport(socketio,message):
    print message
    socketio.emit("progress report", message)
