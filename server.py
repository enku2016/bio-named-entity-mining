from flask import Flask
from flask import request
from subprocess import Popen, PIPE, STDOUT
from flask_socketio import SocketIO
from flask_socketio import send, emit
from flask_cors import CORS
import os 
import requests
import reverb
import POS_tagger
import json
import ConfigParser
import io
import StringIO

app = Flask(__name__,static_url_path='/static')
socketio = SocketIO(app)
dir_path = os.path.dirname(os.path.realpath(__file__))
CORS(app)

# Load the configuration file
with open("config.ini") as f:
    config_file = f.read()
config = ConfigParser.RawConfigParser(allow_no_value=True)
config.readfp(io.BytesIO(config_file))

path_to_grobid = config.get("GROBID","path")
print path_to_grobid

@app.route('/',methods=['GET','POST'])
def serve():
    if request.method == 'POST':
        # get file from request and save in temporary directory for further processing
        pdf_file = request.files['file']
        pdf_file_path = os.path.join(dir_path,'temp',pdf_file.filename)
        pdf_file.save(pdf_file_path)

        IE = Popen(['java', '-jar', 'lib/grobin_test.jar', pdf_file_path,path_to_grobid], stdout=PIPE, stderr=STDOUT)
        pt = processGrobinResult(IE)
        result = reverb.processReverb(pt.name,socketio)
        result = result.replace(" ","")
        IE = Popen(['java', '-jar', 'lib/grobin_test.jar', pdf_file_path, path_to_grobid], stdout=PIPE, stderr=STDOUT)
        taggingResult = processTagging(IE)
        return json.dumps({"Tagging":taggingResult,"scheme":result})
    else:
        return app.send_static_file('index.html')

@app.route('/razor_process',methods=['GET','POST'])
def  razor():
    if request.method == 'POST':
        text = request.form['text']
        taggingResult = processTaggingMemory(text)
        return json.dumps({"Tagging":taggingResult})

def processGrobinResult(IE):
    f = open(dir_path+'/temp/'+'temp_txt_file.txt','w')
    for i in IE.stdout:
        f.write(i)
    f.close()
    return f
def processTagging(IE):
    FinalJSON = []
    for i in IE.stdout:
        if( ">>>>>>>> GROBID_HOME" not in i) and ("Processing /home/" not in i) and ("[Wapiti] Loading model" not in i) and ("Model path:" not in i):
            tokens , pos_tokens , stems , lematization , noun_phrases = POS_tagger.tokenize_words(i)    
            temp_json = {
                'sentence'      : i,
                'token'         : tokens,
                'POS Taggs'     : pos_tokens,
                'stems'         : stems,
                'lematization'  : lematization,
                'noun phrases'  : noun_phrases
            }
            FinalJSON.append(temp_json)
    return FinalJSON 
def processTaggingMemory(text):
    FinalJSON = []
    buffer = StringIO.StringIO(text)
    for i in buffer.readlines():
        tokens , pos_tokens , stems , lematization , noun_phrases = POS_tagger.tokenize_words(i)    
        temp_json = {
            'sentence'      : i,
            'token'         : tokens,
            'POS Taggs'     : pos_tokens,
            'stems'         : stems,
            'lematization'  : lematization,
            'noun phrases'  : noun_phrases
        }
        FinalJSON.append(temp_json)
    return FinalJSON 