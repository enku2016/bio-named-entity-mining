
function prepareUpload(){
    var fd = new FormData();
    $('input[type=file]').on('change', function(event){
        fd.append('file',event.target.files[0])
    });

    $('#button').click(function(e){
        $.ajax({
            type: 'POST',
            url: 'http://localhost:5000',
            data: fd,
            contentType: false,
            enctype: 'multipart/form-data',
            cache: false,
            processData: false,
            async: false,
            success: function(data) {
                console.log(data);
                document.getElementById('result').innerHTML = data;
            },
        });
    })


}

